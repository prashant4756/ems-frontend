/**
 * 
 */

var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {

	var baseUrl="";
	//baseUrl="http://15.152.216.215:8081";
	baseUrl="http://localhost:8081";
	//get all the data
    $http.get(baseUrl+"/EmployeeHibernateAngularJS/HibernateService/allEmployees/")
    .then(function (response) {$scope.employees = response.data;});


	//add a new employee to the database
    $scope.addEmployee = function(){

        alert("inside add employee function "+angular.toJson($scope.form));
		var method="";
		var url="";

		method="POST";
		url=baseUrl+"/EmployeeHibernateAngularJS/HibernateService/allEmployees/";
		$http({
			//configurations here
			method:method,
			url:url,
			data:angular.toJson($scope.form),
			headers : {
               'Content-Type' : 'application/json'
            } 
		}).then(_success, _error)
	}
	

    //delete a perticular employee
    $scope.deleteEmployee = function(employee){
		//alert("delete pressed"+ employee.employeeID);

		var method="";
		var url="";

		method="DELETE";
		url=baseUrl+"/EmployeeHibernateAngularJS/HibernateService/deleteEmployee/";
		$http({
			//configurations here
			method:method,
			url:url,
			data:angular.toJson(employee),
			headers : {
               'Content-Type' : 'application/json'
            } 
		}).then(_successDelete, _error)		
    }

    
    $scope.showEditEmployee = function(employee){

        alert("inside show edit"+employee.employeeId);
		$scope.editData = employee;		
		$scope.editData.employeeId = employee.employeeId;
		$scope.showEditEmployeeForm=true;
		
		//alert("inside show edit employee"+$scope.editData.employeeID); //important
    }
    
    $scope.updateEmployee = function(){
        var method="";
		var url="";

		method="POST";
		url=baseUrl+"/EmployeeHibernateAngularJS/HibernateService/updateEmployee/";
		alert("data -->"+angular.toJson($scope.editData));
		$http({
			//configurations here
			method : method,
			url : url,
			data : angular.toJson($scope.editData),
			headers : {
               'Content-Type' : 'application/json'
            } 
		}).then(_successUpdate, _error)
		
    }
    function _success(response) {
        _refreshEmployeeData();
        _clearFormData()
    }
	function _successDelete(response) {
        _refreshEmployeeData();
        var status = "";
        if(response.data === true)
            status = "deleted successfully"
         else
             status = "cant delete"
        alert(status);
       
    }
	function _successUpdate(response) {
        _refreshEmployeeData();
        $scope.showEditEmployeeForm=false;
        var status = "";
        if(response.data === true)
            status = "Updated successfully"
         else
             status = "cant update"
        alert(status);
       
    }
	function _error(response) {
		alert("error occured!"+response.responseText);
        console.log(response.statusText);
    }
    function _refreshEmployeeData(response){
    	$http.get(baseUrl+"/EmployeeHibernateAngularJS/HibernateService/allEmployees/")
        .then(function (response) {$scope.employees = response.data;});
    		
    }
  	//Clear the form
    function _clearFormData() {
        $scope.form.employeeName="";
        $scope.form.departmentName = "";
        $scope.form.profession = "";
        $scope.form.gender = "";
    
    };

});


